#!/usr/bin/env ruby

@user          = %x(id -u -n).chomp
@globalwarn    = '/etc/news.d/globalwarn'
@globalnews    = '/etc/news.d/global'
@usernews      = "/etc/news.d/#{@user}"
@userscriptdir = "/etc/login.d/#{@user}/"

@red    = "\033[31m"
@green  = "\033[32m"
@yellow = "\033[33m"
@blue   = "\033[34m"
@end    = "\033[0m"

def printheader
	puts "#{@blue}  _  #{@end}#{@green}Welcome to#{@end}#{@blue}  _ _       "
	puts ' | |   _   _  __| (_) __ _ '
	puts ' | |  | | | |/ _  | |/ _` |'
	puts ' | |__| |_| | (_| | | (_| |'
	puts ' |____\__   |\__,_|_|\__,_|'
	puts "       |___/ #{@end}#{@green}Insomnia 24/7#{@end}#{@blue} #{@end}"
	puts ''
end

def sysinfo
	sysinfo   = IO.read('/etc/login.d/sysinfo')
	load      = IO.read('/proc/loadavg')
	ncpu      = sysinfo.split[0].to_f
	sysload   = (load.split[0].to_f/ncpu)*100
	proccount = load.split[3].split('/')[1]
	usercount = %x(w | tail -n +3 | awk '{print $1}' | sort -u | wc -l)
	procmem   = IO.read('/proc/meminfo')
	memtotal  = ''
	memfree   = ''
	memcached = ''
	membuffer = ''
	reclaim   = ''	
	procmem.split("\n").each do | line |
		if line =~ /^MemTotal/
			memtotal = line.split[1]
		elsif line =~ /^MemFree/
			memfree = line.split[1]
		elsif line =~ /^Cached/
			memcached = line.split[1]
		elsif line =~ /^Buffers/
			membuffer = line.split[1]
		elsif line =~ /^SReclaimable/
			reclaim = line.split[1]
		end
	end
	realmemfree = (memfree.to_i + memcached.to_i + membuffer.to_i + reclaim.to_i) / 1024

	puts "CPU            : #{sysinfo}"
	puts "System load    : #{sysload.round(2)}%"
	puts "Process count  : #{proccount}"
	puts "Online users   : #{usercount}"
	puts "Memory total   : #{memtotal.to_i/1024}M"
	puts "Memory free    : #{realmemfree}M"
	puts "Disk total     : #{%x(df -h /home | tail -1 | awk '{print $2}')}"
	puts "Disk free      : #{%x(df -h /home | tail -1 | awk '{print $4}')}"
        puts "Hours of active\nuser sessions\nthis month     : #{%x(ac | awk '{print $2}')}"
end

def userloginscripts
	Dir.entries(@userscriptdir).each do | script |
		if( script == "." || script == ".." )
			next
		end
		load "#{@userscriptdir}#{script}"
	end
	puts ""
end

def printglobalnews
	if(File.exists?(@globalwarn))
		globalnews = ''
			File.open(@globalwarn) do |file|
				file.each do |line|
				globalnews << line
			end
		end
		puts "#{@red}#{globalnews}#{@end}"
	elsif(File.exists?(@globalnews))
		globalnews = ''
			File.open(@globalnews) do |file|
				file.each do |line|
				globalnews << line
			end
		end
		puts "#{@yellow}#{globalnews}#{@end}"
	else
		puts "No global news\n\n"
	end
end

def printusernews
	if(File.exists?(@usernews))
		usernews = ''
			File.open(@usernews) do |file|
				file.each do |line|
				usernews << line
			end
		end
		puts "#{@red}#{usernews}#{@end}"
	else
		puts "No user news\n\n"
	end
end

printheader
sysinfo
userloginscripts
printglobalnews
printusernews
